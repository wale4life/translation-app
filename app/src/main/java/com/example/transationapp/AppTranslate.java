package com.example.transationapp;

import android.app.AlertDialog;
import android.os.AsyncTask;


import java.net.*;
import java.io.*;
import java.util.*;
import com.google.gson.*;
import com.squareup.okhttp.*;

import android.app.ProgressDialog;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Microsoft Translate Import

public class AppTranslate extends AsyncTask<String, String, String> {

    private ProgressDialog p;

    private String subscriptionKey = "65bb344a9501411ea26cef894906b2d3";
    private String url = "https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&to=";

    private TextView outputText;

    public AppTranslate setOutputContext(TextView tv) {
        this.outputText = tv;
        return this;
    }

    // This function performs a POST request.
    // A POST Request is needed to make communication with the azure translation service
    public String process(String text, String langs) throws IOException {

        this.url +=  langs;
        System.out.println(this.url);

        // Instantiates the OkHttpClient.
        // A client is needed to create an http request
        // Http request is how you comment with the internet
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType,
                "[{\n\t\"Text\": \"" + text + "\"\n}]");
        Request request = new Request.Builder()
                .url(this.url)
                .post(body)
                .addHeader("Ocp-Apim-Subscription-Key", this.subscriptionKey)
                .addHeader("Content-type", "application/json").build();
        Response response = client.newCall(request).execute();
        String res = prettify(response.body().string());

        return res;
    }

    // This function prettifies the json response.
    private static String prettify(String json_text) {
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(json_text);
        JsonArray jsonArray = jsonElement.getAsJsonArray();

        // Convert JSON Response to String
        String text = "";
        try {
            JsonObject obj = jsonArray.get(0).getAsJsonObject();
            text = String.format(obj.get("translations").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString());
        }catch( Exception e) { System.err.println(e); }

        return text;
    }

    @Override
    protected String doInBackground(String... texts) {
        String langs = texts[1];
        String res = "";
        String text = texts[0];

        try {
            res = this.process(text, langs);
        } catch (IOException x) { System.err.println(x); }

        return res;
    }

    @Override
    protected void onPostExecute(String tv) {
        super.onPostExecute(tv);
        System.out.println(tv);
        Log.println(Log.INFO, tv, tv);

        if(this.outputText == null)
         new AlertDialog.Builder(MainActivity.context).setMessage(tv).show();
        else {
            outputText.setText("TRANSLATED TEXT: \n" + tv);
        }
    }
}
