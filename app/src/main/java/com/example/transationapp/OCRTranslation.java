package com.example.transationapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.microsoft.projectoxford.vision.contract.Line;
import com.microsoft.projectoxford.vision.contract.OCR;
import com.microsoft.projectoxford.vision.contract.Region;
import com.microsoft.projectoxford.vision.contract.Word;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import io.grpc.internal.IoUtils;


public class OCRTranslation {

    // Replace <Subscription Key> with your valid subscription key.
    private final String subscriptionKey = "5a939f0ce2a64dd1ae4640e778debe3f";
    private final String uriBase = "https://eastus.api.cognitive.microsoft.com/vision/v1.0/ocr";

    public OCRTranslation() {}

    public void process(final byte[] ins) {

        @SuppressLint("StaticFieldLeak") AsyncTask<byte[], String, String> recText = new AsyncTask<byte[], String, String>() {
            ProgressDialog pb = new ProgressDialog(MainActivity.context);
            @Override
            protected String doInBackground(byte[]... params) {
                byte[] motherfucker = params[0];
                // Show progress loading
                publishProgress("Recognising");
                try {

                    // Initiate an http request instance
                    OkHttpClient client = new OkHttpClient();

//                  String content = String.format("{ \"url\": \"http://www.apache.org/images/asf_logo_wide.gif\" }");
                    RequestBody body = RequestBody.create(MediaType.parse("application/octet-stream"), motherfucker);

                    // Request parameters.
                    Request request = new Request.Builder()
                            .url(uriBase + "?language=en&detectOrientation=true&mode=printed")
                            .post(body)
                            .addHeader("Ocp-Apim-Subscription-Key", subscriptionKey)
                            .addHeader("Content-Type", "application/octet-stream").build();
                    Response response = client.newCall(request).execute();

                    if(response.code() != 200) return "Something went wrong :( ... " + response ;
                    String resStr = prettify(response.body().string());

                    // Convert the returned data to a readable json string
                    return prettify(resStr);
                } catch( Exception e) {
                    System.out.println(e);
                    return null;
                }
            }
            
            protected  void onPreExecute() { pb.show(); }
            protected  void onPostExecute(String s) {
                super.onPostExecute(s);
                pb.dismiss();
                OCR ocr = new Gson().fromJson(s, OCR.class);
                StringBuilder sb = new StringBuilder();

                // Retrieve all text from the return
                for(Region region : ocr.regions) {
                    for(Line line : region.lines)
                        for(Word word : line.words)
                            sb.append(word.text + "\n");
                }

                new AlertDialog.Builder(MainActivity.context).setMessage(sb.toString()).show(); //Display image retrieved texts
                new AppTranslate().execute(sb.toString(), "fr", null, null); // Perform and Display translated text
            }
        };
        recText.execute(ins);
    }

    private static String prettify(String json_text) {
        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(json_text);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(json);
    }
}


