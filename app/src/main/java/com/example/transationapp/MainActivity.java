package com.example.transationapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import com.fasterxml.jackson.core.util.BufferRecycler;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.android.gms.vision.text.internal.client.TextRecognizerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.transationapp.AppTranslate;
import com.google.api.client.util.IOUtils;
import com.google.common.primitives.Bytes;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Function;

import io.grpc.internal.IoUtils;

public class MainActivity extends AppCompatActivity {
    private TextView mTextMessage;
    public static MainActivity context;
    private final int PICK_IMAGE = 22;
    public SurfaceView camera = null;
    public final int RequestCameraPermissionID = 1001;
    public TextView cameraText = null;
    public TextView cameraTranslatedText = null;
    CameraSource cameraSource;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        BottomNavigationView navView = findViewById(R.id.nav_view);
        mTextMessage = findViewById(R.id.message);
//        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        new AlertDialog.Builder(this).setMessage("Welcome to translation app").show();
        this.btnCameraClick(); // Start the Translation camera
        cameraText = (TextView)findViewById(R.id.cameraText);
        cameraTranslatedText = (TextView)findViewById(R.id.cameraTranslatedText);
    }

    public void btnTranslateOnClick(View v) {
        final TextView tv = (TextView) findViewById(R.id.textResult);
        EditText entered = (EditText) findViewById(R.id.editText2);

        MainActivity.context = this;

        try {
            String text = entered.getText().toString();
            System.out.println(text);
            try {
//                String translated = new AppTranslate().process("We are coming", "en,fr");
                new AppTranslate().execute(text, "fr", null, null);

//                System.out.println(translated);

//                new AlertDialog.Builder(MainActivity.this)
//                        .setMessage(translated).show();
            } catch (Exception e) {
                System.err.println(e);
//                new AlertDialog.Builder(MainActivity.this)
//                        .setMessage(e.getMessage()).show();
            }
        } catch (Exception e) {
            new AlertDialog.Builder(MainActivity.this)
                    .setMessage("Translation Failed " + e).show();
            System.out.println(e);
        }
    }

    /**
     * Performs Image Upload
     *
     * @param view
     */
    public void btnUploadImageClick(View view) {

        // Create Display area for image selection
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Image to translate"), PICK_IMAGE);
    }


    /**
     * Performs Image Upload
     *
     */
    public void btnCameraClick() {
        camera = (SurfaceView) findViewById(R.id.cameraview);
        TextRecognizer txtRecognizer = new TextRecognizer.Builder(this.getApplicationContext()).build();

        if (!txtRecognizer.isOperational())
            Log.w("MainActivity", "detector dependencies are not yet available");
        else {
            cameraSource = new CameraSource.Builder(getApplicationContext(), txtRecognizer).setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setRequestedFps(60.0f)
                    .setAutoFocusEnabled(true)
                    .build();
            camera.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder surfaceHolder) {
                    try {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA},
                                    RequestCameraPermissionID);
                            return;
                        }
                        cameraSource.start(camera.getHolder());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

//              @Override
                public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) { }


                @Override
                public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                    cameraSource.stop();
                }
            });
            txtRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() { }

                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if (items.size() != 0) {
                        cameraText.post(new Runnable() {
                            @Override
                            public void run() {
                                final StringBuilder sb = new StringBuilder();
                                for (int j = 0; j < items.size(); j++) {
                                    TextBlock item = items.valueAt(j);
                                    sb.append(item.getValue());
                                    sb.append("\n");
                                }
                                cameraText.setText("DETECTED TEXT: \n" + sb.toString());
                                cameraTranslatedText.post(new Runnable() {
                                    @Override
                                    public void run() {
                                    new AppTranslate().setOutputContext(cameraTranslatedText).execute(sb.toString(), "fr", null, null);
                                    }
                                });
                            }
                        });


                    }
                }
            });
        }
    }

    /**
     * Retrieve the selected image for upload
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Confirm the selected image
        if (resultCode == PICK_IMAGE || true) {
            if (resultCode != Activity.RESULT_OK || data == null)
                return; // Check whether image upload was cancelled

            try {
                //Retrieve the image data stream
                // Convert image data stream to bitmap image data so it can be readable
                InputStream ins = context.getContentResolver().openInputStream(data.getData());

                ImageView imgArea = (ImageView) findViewById(R.id.imageView);

                // Convert Image data to byte array stream so its save to send over http
                byte[] bytes = IoUtils.toByteArray(ins);
                Bitmap bmp = MediaStore.Images.Media.getBitmap(context.getContentResolver(), data.getData());

                // Display image on the app
                imgArea.setImageBitmap(bmp);

                // Process image to read the text content
                new OCRTranslation().process(bytes);
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RequestCameraPermissionID:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                        return;
                    try {
                        cameraSource.start(camera.getHolder());
                    } catch (Exception e) {
                        System.err.println(e);
                    }
                }
        }
    }

}


